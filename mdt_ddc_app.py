#!/usr/bin/env python3

import streamlit as st
import pandas as pd
import numpy as np
import datetime

st.write("""
# MDT DAQ-DCS Offline Data Viewer                
""")


# to do : select the runNumber from the drop list
runNumber = st.text_input("Run Number", 476929)
st.write("The current ddc info is for run ", runNumber)


ddc_data = f'run{runNumber}_DDC.csv'
ddc_df= pd.read_csv(ddc_data,low_memory=False)

#st.dataframe(ddc_df)
# Filter rows where 'element_name' contains "DisabledRod" and value == 1
mroddrop_df = ddc_df[ddc_df['element_name'].str.contains("DisabledRod") & (ddc_df['value_number'] == 1)]

st.write(f"Total {mroddrop_df.shape[0]} chamber dropped due to mrod stopless removal during run ", runNumber)
st.dataframe(mroddrop_df)

# Filter rows where 'element_name' contains "Pathologic" and value == 1
pathologic_df = ddc_df[ddc_df['element_name'].str.contains("Pathologic") & (ddc_df['value_number'] == 1)]

st.write(f"Total {pathologic_df.shape[0]} chamber decleared pathologic during run ", runNumber)
st.dataframe(pathologic_df)

# Filter rows where 'element_name' contains "Recovering" and value == 1
autorecovery_df = ddc_df[ddc_df['element_name'].str.contains("Recovering") & (ddc_df['value_number'] == 1)]

st.write(f"Total {autorecovery_df.shape[0]} auto recoveries during run ", runNumber)
st.dataframe(autorecovery_df)

# Filter rows where 'element_name' contains "Dropped" and value == 1
dropped_df = ddc_df[ddc_df['element_name'].str.contains("Dropped") & (ddc_df['value_number'] == 1)]

st.write(f"Total {dropped_df.shape[0]} Chambers/Mezz cards drops during run ", runNumber)
st.dataframe(dropped_df)

# draw recovery with time stamp + run + lumi info


