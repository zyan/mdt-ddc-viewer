import streamlit as st
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.cbook as cbook
import os
import re
from datetime import datetime, timedelta

st.set_page_config(layout="wide")

st.write("""
# MDT DAQ-DCS Offline Data Viewer - Dev Change
""")

def file_selector(folder_path):
    filenames = [x[-14:-8] for x in os.listdir(folder_path) if x[-8:] == '_DDC.csv']
    selected_filename = st.selectbox('Select a run number', filenames)
    return os.path.join(folder_path, 'run' + selected_filename + '_DDC.csv')

#local folder
#data_path = file_selector('/Users/yanyinrui/DCS_Data/')

#deploy eos folder
data_path = file_selector('/eos/user/m/mdtoffline/SWAN_projects/DCS_Data/')

st.write('You selected `%s`' % data_path)

runNumber = data_path[-14:-8]

#Run number selection
#option = st.selectbox(
#    "Select run number for DDC info",
#    (runNumber, 400000, 410000))

#runNumber = st.text_input("Run number", 476929)
#st.write("DDC info for run ", runNumber)

#for deployment

#folder = '/eos/user/m/mdtoffline/SWAN_projects/DCS_Data/'
#ddc_data = f'{folder}run{runNumber}_DDC.csv'

ddc_df= pd.read_csv(data_path,low_memory=False,index_col=0)

#st.dataframe(ddc_df)
# Filter rows where 'element_name' contains "DisabledRod" and value == 1
mroddrop_df = ddc_df[ddc_df['element_name'].str.contains("DisabledRod") & (ddc_df['value_number'] == 1)]
# Filter rows where 'element_name' contains "Pathologic" and value == 1
pathologic_df = ddc_df[ddc_df['element_name'].str.contains("Pathologic") & (ddc_df['value_number'] == 1)]

# Filter rows where 'element_name' contains "Recovering" and value == 1
autorecovery_df = ddc_df[ddc_df['element_name'].str.contains("Recovering") & (ddc_df['value_number'] == 1)]


st.write("Chamber auto recovery timeline")
ar_timeline = autorecovery_df.drop(columns=['value_number'])
ar_timeline['chamber_name'] = ar_timeline['element_name'].apply(lambda x :x.split('.')[0][-7:])
ar_timeline = ar_timeline.drop(columns=['element_name'])
ar_timeline['level'] = [np.random.randint(-6,-1) if (i%2)==0 else np.random.randint(1,6) for i in range(len(ar_timeline))]
ar_timeline['ts'] = ar_timeline['ts'].str.slice(0,19)
ar_timeline['ts'] = pd.to_datetime(ar_timeline['ts'], format = '%Y-%m-%d %H:%M:%S')

#ar_timeline
st.dataframe(ar_timeline)

#plotting the timeline
fig, ax = plt.subplots(figsize=(18,9))

#hour_row = {'ts': ar_timeline.iloc[0].ts.round('H'), 'chamber_name': 'XXXXXXX', 'level': 0}
#df1 = pd.DataFrame(hour_row, index=[0])
#frame = [df1, ar_timeline]
#hour_round = pd.concat(frame)
#hour_round

ax.plot(ar_timeline.ts, [0,]* len(ar_timeline), "-o", color="black", markerfacecolor="white")

ax.set_xticks(pd.date_range(ar_timeline.iloc[0].ts.floor('H'), ar_timeline.iloc[-1].ts.ceil('H'), freq="h"))
ax.xaxis.set_major_formatter(mdates.DateFormatter('%H:%M'))
ax.set_ylim(-7,7)

for idx in range(ar_timeline.shape[0]):
    dt = ar_timeline.iloc[idx].ts
    chamber = ar_timeline.iloc[idx].chamber_name
    level = ar_timeline.iloc[idx].level
    dt_str = dt.strftime("%H:%M")
    ax.annotate(dt_str + "\n" + chamber, xy=(dt, 0.1 if level>0 else -0.1),xytext=(dt, level),
                arrowprops=dict(arrowstyle="-",color="red", linewidth=0.8),
                ha="center")

ax.spines[["left", "top", "right", "bottom"]].set_visible(False)
ax.spines[["bottom"]].set_position(("axes", 0.5))
ax.yaxis.set_visible(False)

st.pyplot(plt)

st.write(f"A total of {mroddrop_df.shape[0]} chambers dropped due to mrod stopless removal during run ", runNumber)
if mroddrop_df.shape[0]:
    st.dataframe(mroddrop_df)

st.write(f"A total of {autorecovery_df.shape[0]} chambers went into auto recovery during run ", runNumber)
if autorecovery_df.shape[0]:
    st.dataframe(autorecovery_df)

st.write(f"A total of {autorecovery_df.shape[0]} chambers went into auto recovery during run ", runNumber)
if autorecovery_df.shape[0]:
    st.dataframe(autorecovery_df)

# Filter rows where 'element_name' contains "Dropped" and value == 1
dropped_df = ddc_df[ddc_df['element_name'].str.contains("Dropped") & (ddc_df['value_number'] == 1)]

st.write(f"A total of {dropped_df.shape[0]} chambers/mezz cards dropped during run ", runNumber)
if dropped_df.shape[0]:
    st.dataframe(dropped_df)

# draw recovery with time stamp + run + lumi info
#st.image(f"https://atlasdaq.cern.ch/info/mda/get/MDTGnam/r0000{runNumber}_lEoR_ATLAS_MDT-MDA-Monitoring_MDT.root//Histogramming-MDT/MDT-BA01-GnamMon/EXPERT/MDT/BIL1A03/HitsPerChannel",width=400)